import {createMuiTheme} from "@material-ui/core";

export default function useMuiTheme() {
    return createMuiTheme({
        spacing: 4,

        breakpoints: {
            values: {
                sm: 700
            }
        },

        palette: {
            primary: {
                main: '#0F4780'
            },
            secondary: {
                main: '#6B64FF'
            }
        },

        overrides: {
            MuiButton: {
                root: {
                    width: '100%',
                },
                text: {
                    padding: '10px 16px'
                },
                containedSecondary: {
                    "&:hover": {
                        opacity: 0
                    }
                },
                label: {
                    fontSize: '16px',
                    fontWeight: "bold",
                }
            },
            MuiPaper: {
                rounded: {
                    borderRadius: '8px'
                }
            }
        }
    })
}
