import {makeStyles} from "@material-ui/core";

export const useStyles = makeStyles((theme) => ({
    red: {
        color: theme.palette.error.main,
        background: theme.palette.error.main,
    }
}));
