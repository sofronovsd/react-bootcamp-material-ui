import {Button} from "@material-ui/core";
import {useStyles} from "./CustomButton.styles";
import clsx from "clsx";

export const CustomButton = ({error, ...rest}) => {
    const classes = useStyles();
    return (
        <Button
            variant="contained"
            className={clsx(error && classes.red)}
            {...rest}
        >
        </Button>
    )
}
