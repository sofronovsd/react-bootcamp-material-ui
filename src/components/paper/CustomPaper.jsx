import {useStyles} from "./CustomPaper.styles";
import {Paper} from "@material-ui/core";

export const CustomPaper = (props) => {
    const classes = useStyles();
    return (
        <Paper
            className={classes.customPaper}
            {...props}
        >
        </Paper>
    )
}
