import {makeStyles} from "@material-ui/core";

export const useStyles = makeStyles((theme) => ({
    customPaper: {
        width: '400px',
        padding: '20px'
    }
}));
