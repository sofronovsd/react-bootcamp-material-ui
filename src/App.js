import './App.css';
import useMuiTheme from "./themes/useMuiTheme";
import {MuiThemeProvider} from "@material-ui/core/styles";
import {CustomPaper} from "./components/paper/CustomPaper";
import {Box, Grid} from "@material-ui/core";
import {CustomButton} from "./components/button/CustomButton";

function App() {
    const theme = useMuiTheme();

    return (
        <MuiThemeProvider theme={theme}>
            <Box paddingLeft={4} paddingRight={4}>
                <Grid
                    container
                    justify="center"
                    alignItems="center"
                    style={{minHeight: '100vh'}}
                >
                    <CustomPaper>
                        <Grid container spacing={4}>
                            <Grid item xs={12} sm={6}>
                                <CustomButton color="primary">Button 1</CustomButton>
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <CustomButton color="secondary">Button 2</CustomButton>
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <CustomButton color="primary">Button 3</CustomButton>
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <CustomButton error>Button 4</CustomButton>
                            </Grid>
                        </Grid>
                    </CustomPaper>
                </Grid>
            </Box>
        </MuiThemeProvider>
    );
}

export default App;
